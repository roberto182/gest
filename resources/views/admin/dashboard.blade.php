@extends('admin.layout.master')
@section('content')
    <?php ini_set('memory_limit', '2048M'); ?>

    <div class="container-fluid">
        <div class="card">
            <div class="card-header" id="headingOne">
                <div class="row">
                    <div class="col text-center">
                        <h4>
                            <span>Produtos</span>
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h5 class="text-center">
                            <a href="#" id="new" class="badge badge-success" onclick="newProduct()">
                                Novo Registro
                            </a>
                        </h5>
                    </div>
                    <div class="col">
                        <h5 class="text-center">
                            <a href="#" id="import" class="badge badge-primary" onclick="importProducts()">
                                Importar Arquivo <i class="fa fa-file-excel-o"></i>
                            </a>
                        </h5>
                    </div>
                    <div class="col">
                        <h5 class="text-center">
                            <a href="#" id="export" class="badge badge-info" onclick="exportProducts()">
                                Exportar Arquivo<i class="fa fa-file-excel-o"></i>
                            </a>
                        </h5>
                    </div>
                </div>
            </div>

            <div class="card-body" id="card_tabela">
                <div class="row">
                    <div class="col">
                        <div id="calendar"></div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table id="products_table"
                           class="table table-sm table-hover table-bordered table-striped dataTable no-footer products_table">
                        <thead>
                        <tr>
                            <th class="text-center"><input name="select_all" value="1" type="checkbox"></th>
                            <th class="text-center">Código</th>
                            <th class="text-center">Descrição</th>
                            <th class="text-center">Und</th>
                            <th class="text-center">P Custo</th>
                            <th class="text-center">V VENDA</th>
                            <th class="text-center">Detalhes 1</th>
                            <th class="text-center">Detalhes 2</th>
                            <th class="text-center">Estoque</th>
                            <th class="text-center">Ação</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ModalDetail" tabindex="-1" role="dialog"
         aria-labelledby="ModalDetailTitle" aria-hidden="true">
        <div class="modal-dialog modal-xg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalDetailTitle">Detalhes do Item</h5>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                {{--modal body--}}
                <div class="modal-body">
                    <div class="alert alert-simples">

                        <div class="row">
                            <div class="form-group col-md-2">
                                <label class="form-label" for="code_detail">Código</label>
                                <input class="form-control" id="code_detail" type="text" name="code" readonly>
                            </div>
                            <div class="form-group col-md-8">
                                <label class="form-label" for="description_detail">Descrição</label>
                                <input class="form-control" id="description_detail" type="text" name="description" placeholder="Informe uma descrição" readonly>
                            </div>
                            <div class="form-group col-md-2">
                                <label class="form-label" for="unity_detail">Unidade</label>
                                <input class="form-control" id="unity_detail" type="text" name="unity" placeholder="Informe uma unidade" readonly>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-8">
                                <label class="form-label" for="model_one_detail">Detalhes 1</label>
                                <input class="form-control" id="model_one_detail" type="text" name="model_one" placeholder="Detalhes 1" readonly>
                            </div>
                            <div class="form-group col-2">
                                <label class="form-label" for="price_cost_detail">Preço de Custo</label>
                                <input class="form-control valor" id="price_cost_detail" type="text" placeholder="R$" name="price_cost" readonly>
                            </div>
                            <div class="form-group col-2">
                                <label class="form-label" for="unitary_value_detail">Valor venda</label>
                                <input class="form-control valor" id="unitary_value_detail" type="text" name="unitary_value" placeholder="R$" onKeyPress="return(moeda(this,'.',',',event))" readonly>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-8">
                                <label class="form-label" for="model_two_detail">Detalhes 2</label>
                                <input class="form-control" id="model_two_detail" type="text" name="model_two" placeholder="Detalhes 2" readonly>
                            </div>
                            <div class="form-group col-md-2">
                                <label class="form-label" for="code_for_detail">Cod For</label>
                                <input class="form-control" id="code_for_detail" type="text" name="code_for" placeholder="Cod for" readonly>
                            </div>
                            <div class="form-group col-md-2">
                                <label class="form-label" for="code_gru_detail">Cod GRU</label>
                                <input class="form-control" id="code_gru_detail" type="text" name="code_gru" placeholder="Cod GRU" readonly>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-8">
                                <label class="form-label" for="name_gru_detail">Nome GRU</label>
                                <input class="form-control" id="name_gru_detail" type="text" name="name_gru" placeholder="Nome GRU" readonly>
                            </div>
                            <div class="form-group col-md-2">
                                <label class="form-label" for="minimum_amount_detail">Estoque</label>
                                <input class="form-control" id="minimum_amount_detail" type="number" min="0" name="minimum_amount" placeholder="Estoque" readonly>
                            </div>
                            <div class="form-group col-md-2">
                                <label class="form-label" for="weight_detail">Peso</label>
                                <input class="form-control" id="weight_detail" type="text" name="weight" placeholder="Peso" readonly>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ModalNewProduct" tabindex="-1" role="dialog"
         aria-labelledby="ModalNewProductTitle" aria-hidden="true">
        <div class="modal-dialog modal-xg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalNewProductTitle">Novo Item Produto</h5>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                {{--modal body--}}
                <div class="modal-body">
                    <form id="form_new" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="form-group col-md-10">
                                <label class="form-label" for="description">Descrição</label>
                                <input class="form-control" id="description" type="text" name="description" placeholder="Informe uma descrição">
                            </div>
                            <div class="form-group col-md-2">
                                <label class="form-label" for="unity">Unidade</label>
                                <input class="form-control" id="unity" type="text" name="unity" placeholder="Informe uma unidade">
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-md-8">
                                <label class="form-label" for="model_one">Detalhes 1</label>
                                <input class="form-control" id="model_one" type="text" name="model_one" placeholder="Detalhes 1">
                            </div>
                            <div class="form-group col-2">
                                <label class="form-label" for="price_cost">Preço de Custo</label>
                                <input class="form-control valor" id="price_cost" type="text" name="price_cost" placeholder="R$">
                            </div>
                            <div class="form-group col-2">
                                <label class="form-label" for="unitary_value">Valor venda</label>
                                <input class="form-control valor" id="unitary_value" type="text" name="unitary_value" placeholder="R$">
                            </div>
                        </div>
                        
                        <div class="row"> 
                            <div class="form-group col-md-8">
                                <label class="form-label" for="model_two">Detalhes 2</label>
                                <input class="form-control" id="model_two" type="text" name="model_two" placeholder="Detalhes 2">
                            </div>
                            <div class="form-group col-md-2">
                                <label class="form-label" for="code_for">Cod For</label>
                                <input class="form-control" id="code_for" type="text" name="code_for" placeholder="Cod for">
                            </div>
                            <div class="form-group col-md-2">
                                <label class="form-label" for="code_gru">Cod GRU</label>
                                <input class="form-control" id="code_gru" type="text" name="code_gru" placeholder="Cod GRU">
                            </div>
                        </div>
                        
                        <div class="row"> 
                            <div class="form-group col-md-8">
                                <label class="form-label" for="name_gru">Nome GRU</label>
                                <input class="form-control" id="name_gru" type="text" name="name_gru" placeholder="Nome GRU">
                            </div>
                            <div class="form-group col-md-2">
                                <label class="form-label" for="minimum_amount">Estoque</label>
                                <input class="form-control" id="minimum_amount" type="number" min="0" name="minimum_amount" placeholder="Estoque">
                            </div>
                            <div class="form-group col-md-2">
                                <label class="form-label" for="weight">Peso</label>
                                <input class="form-control" id="weight" type="text" name="weight" placeholder="Peso">
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Salvar</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>

    <div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog"
         aria-labelledby="ModalUpdateTitle" aria-hidden="true">
        <div class="modal-dialog modal-xg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalUpdateTitle">Atualizar Item</h5>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                {{--modal body--}}
                <form id="form_update" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" id="product_item" name="id">
                    <div class="modal-body">
                        <div class="alert alert-simples">

                            <div class="row">
                                <div class="form-group col-md-2">
                                    <label class="form-label" for="code_update">Código</label>
                                    <input class="form-control" id="code_update" type="text" name="code">
                                </div>
                                <div class="form-group col-md-8">
                                    <label class="form-label" for="description_update">Descrição</label>
                                    <input class="form-control" id="description_update" type="text" name="description" placeholder="Informe uma descrição">
                                </div>
                                <div class="form-group col-md-2">
                                    <label class="form-label" for="unity_update">Unidade</label>
                                    <input class="form-control" id="unity_update" type="text" name="unity" placeholder="Informe uma unidade">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-8">
                                    <label class="form-label" for="model_one_update">Detalhes 1</label>
                                    <input class="form-control" id="model_one_update" type="text" name="model_one" placeholder="Detalhes 1">
                                </div>
                                <div class="form-group col-2">
                                    <label class="form-label" for="price_cost_update">Preço de Custo</label>
                                    <input class="form-control valor" id="price_cost_update" type="text" placeholder="R$" name="price_cost">
                                </div>
                                <div class="form-group col-2">
                                    <label class="form-label" for="unitary_value_update">Valor venda</label>
                                    <input class="form-control valor" id="unitary_value_update" type="text" name="unitary_value" placeholder="R$">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-8">
                                    <label class="form-label" for="model_two_update">Detalhes 2</label>
                                    <input class="form-control" id="model_two_update" type="text" name="model_two" placeholder="Detalhes 2">
                                </div>
                                <div class="form-group col-md-2">
                                    <label class="form-label" for="code_for_update">Cod For</label>
                                    <input class="form-control" id="code_for_update" type="text" name="code_for" placeholder="Cod for">
                                </div>
                                <div class="form-group col-md-2">
                                    <label class="form-label" for="code_gru_update">Cod GRU</label>
                                    <input class="form-control" id="code_gru_update" type="text" name="code_gru" placeholder="Cod GRU">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-8">
                                    <label class="form-label" for="name_gru_update">Nome GRU</label>
                                    <input class="form-control" id="name_gru_update" type="text" name="name_gru" placeholder="Nome GRU">
                                </div>
                                <div class="form-group col-md-2">
                                    <label class="form-label" for="minimum_amount_update">Estoque</label>
                                    <input class="form-control" id="minimum_amount_update" type="number" min="0" name="minimum_amount" placeholder="Estoque">
                                </div>
                                <div class="form-group col-md-2">
                                    <label class="form-label" for="weight_update">Peso</label>
                                    <input class="form-control" id="weight_update" type="text" name="weight" placeholder="Peso">
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Confirmar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ModalRemoveItem" tabindex="-1" role="dialog"
         aria-labelledby="ModalRemoveItemTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalRemoveItemTitle">Remover Item</h5>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                {{--modal body--}}
                <form id="form_delete" method="post">
                    <input type="hidden" id="item_delete" name="item_product">
                    <div class="modal-body">
                        <div class="alert alert-danger">
                            <b>Descrição: </b><span id="description_item"></span>
                            <br><b>Deseja realmente remover o item?</b>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Confirmar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <form id="form_export_products" method="POST">

    </form>

    
@endsection


@section('myscript')

    <script>
        $(document).ready(function () {
            //tabela_valor
            $('.modal').on('hidden.bs.modal', function () {
                $('.tabela_valor').each(function () {
                    $(this).text('');
                });
            })
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
            productsSeletec();
        });

        function updateDataTableSelectAllCtrl(table) {

            var $table = table.table().node();
            var $chkbox_all = $('tbody input[type="checkbox"]', $table);
            var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
            var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

            // If none of the checkboxes are checked
            if ($chkbox_checked.length === 0) {
                chkbox_select_all.checked = false;
                if ('indeterminate' in chkbox_select_all) {
                    chkbox_select_all.indeterminate = false;
                }

                // If all of the checkboxes are checked
            } else if ($chkbox_checked.length === $chkbox_all.length) {
                chkbox_select_all.checked = true;
                if ('indeterminate' in chkbox_select_all) {
                    chkbox_select_all.indeterminate = false;
                }

                // If some of the checkboxes are checked
            } else {
                chkbox_select_all.checked = true;
                if ('indeterminate' in chkbox_select_all) {
                    chkbox_select_all.indeterminate = true;
                }
            }
        }

        var rows_selected_products = [];

        function productsSeletec() {

            var table = $('#products_table').DataTable({
                "processing": true,
                "serverSide": true,
                "autoWidth": false,
                "ajax": "/adm/products/getdata",
                "pageLength": 50,
                'order': [0, 'DESC'],
                'columnDefs': [
                    {
                        'targets': 0,
                        'searchable': false,
                        'orderable': false,
                        'width': '1%',
                        'className': 'dt-body-center',
                        'render': function(data, type, full, meta) {
                            return '<input type="checkbox">';
                        }
                    },
                    {
                        "targets": [0, 1, 2, 3, 4, 5, 6, 7, 8],
                        "className": "text-center",
                    },
                    {
                        "targets": 9,
                        "className": "no-select",
                    },
                    {
                        "targets": [0,3,4,5,6,7,8,9],
                        'searchable': false,
                    },
                    {
                        "targets": [4,5],
                        render: function(data) {
                            return 'R$ ' + numberParaReal(parseInt(data));
                        }
                    },
                ],
                'rowCallback': function(row, data, dataIndex) {
                    // Get row ID
                    //var rowId = data[0];
                    var rowId = data.id;

                    // If row ID is in the list of selected row IDs
                    if ($.inArray(rowId, rows_selected_products) !== -1) {

                        $(row).find('input[type="checkbox"]').prop('checked', true);
                        $(row).addClass('selected');
                    }
                },
                "columns": [
                    {"data": "id"},
                    {"data": "code"},
                    {"data": "description"},
                    {"data": "unity"},
                    {"data": "price_cost"},
                    {"data": "unitary_value"},
                    {"data": "model_one"},
                    {"data": "model_two"},
                    {"data": "minimum_amount"},
                    {"data": "action"},
                ]
            });


            $('#products_table tbody').on('click', 'input[type="checkbox"]', function(e) {
                var $row = $(this).closest('tr');
                // Get row data
                var data = table.row($row).data();
                // Get row ID
                var rowId = data.id;


                // Determine whether row ID is in the list of selected row IDs
                var index = $.inArray(rowId, rows_selected_products);

                // If checkbox is checked and row ID is not in list of selected row IDs
                if (this.checked && index === -1) {
                    rows_selected_products.push(rowId);

                    // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
                } else if (!this.checked && index !== -1) {
                    rows_selected_products.splice(index, 1);
                }

                if (this.checked) {
                    $row.addClass('selected');
                } else {
                    $row.removeClass('selected');
                }


                // Update state of "Select all" control
                updateDataTableSelectAllCtrl(table);

                // Prevent click event from propagating to parent
                e.stopPropagation();


                setInterval(function() {
                    if (rows_selected_products.length) {
                        $('#designar').prop('disabled', false);
                    } else {
                        $('#designar').prop('disabled', true);
                    }
                }, 100);

            });

            // Handle click on table cells with checkboxes
            $('#products_table').on('click', 'tbody td, thead th:first-child', function(e) {

                if(!$(this).hasClass('no-select')){
                    $(this).parent().find('input[type="checkbox"]').trigger('click');
                }
            });

            // Handle click on "Select all" control
            $('thead input[name="select_all"]', table.table().container()).on('click', function(e) {
                if (this.checked) {
                    $('.products_table tbody input[type="checkbox"]:not(:checked)').trigger('click');
                } else {
                    $('.products_table tbody input[type="checkbox"]:checked').trigger('click');
                }

                // Prevent click event from propagating to parent
                e.stopPropagation();
            });

            // Handle table draw event
            table.on('draw', function() {
                // Update state of "Select all" control
                updateDataTableSelectAllCtrl(table);
            });


        }

        function importProducts() {
            alert('sem acesso')
        }

        function exportProducts() {
            // $('#form_export_products').submit();
            alert('sem acesso')
        }

        $('#form_export_products').validate({
            onkeyup: function(element) {
                $(element).valid();
            },
            errorClass: "error",
            rules: {
                "id[]": {
                    required: true
                }
            },
            messages: {
                "id[]": {
                    required: "Por favor, selecione um produto.",
                }
            },
            submitHandler: function(form) {

                $.each(rows_selected_products, function(index, rowId) {

                    $('#form_export_products').append(
                        $('<input>')
                            .attr('type', 'hidden')
                            .attr('id', rowId)
                            .attr('name', 'id[]')
                            .val(rowId)
                    );
                });

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "/adm/export/products",
                    method: "POST",
                    data: $('#form_export_products').serialize(),
                    success: function(data) {
                        console.log(data)

                        // toastr.success('Inscrição designada com sucesso!', 'Sucesso!', {
                        //     timeOut: 3000
                        // });
                        //
                        // rows_selected_sem = []
                        // $('input[name="inscricao_id\[\]"]').each(function() {
                        //     $('#' + this.id).remove();
                        // });
                        //
                        // $.each($(".candidatos_area"), function() {
                        //     $('#' + this.id).DataTable().ajax.reload(null, false);
                        // });
                        //
                        // $.each($(".candidatos_area_com"), function() {
                        //     $('#' + this.id).DataTable().ajax.reload(null, false);
                        // });

                    },
                    error: function(data) {

                    }
                });
            }
        });

        function newProduct(){
            $('#ModalNewProduct').modal('show');
        }

        function drop(id){
            $('#ModalRemoveItem').modal('show');
            $('#item_delete').val(id);
            $('#description_item').text($('#delete_'+id).data('description'));
        }

        $('#form_delete').on('submit', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: '/adm/delete/product',
                data: $('#form_delete').serialize(),
                success: function(data) {

                    $('#products_table').DataTable().ajax.reload(null, false);
                    toastr.success('Informação removida com sucesso!', 'Sucesso!', {
                        timeOut: 3000
                    });

                    $('body').loadingModal('destroy');

                    $('.modal').modal('hide');

                },
                error: function(data) {
                    toastr.error('Não foi possível remover!', 'Erro!', {
                        timeOut: 3000
                    });
                    $('body').loadingModal('destroy');
                }
            });
        });

        $('#form_new').validate({
            onkeyup: function (element) {
                $(element).valid();
            },
            errorClass: "error",
            rules: {
                description: {
                    required: true,
                    minlength: 3
                },
                price_cost: {
                    required: true,
                },
                unitary_value: {
                    required: true,
                },
                minimum_amount: {
                    required: true,
                },
            },
            messages: {
                description: {
                    required: "Por favor, informe uma descrição",
                    minlength: "A descrição deve ter no mínimo 3 caracteres."
                },
                price_cost: {
                    required: "Por favor, informe o preço de custo.",
                },
                unitary_value: {
                    required: "Por favor, informe Valor venda.",
                },
                minimum_amount: {
                    required: "Por favor, a Estoqueima.",
                },

            },
            submitHandler: function (form) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: '/adm/store/product',
                    data: $('#form_new').serialize(),

                    success: function (data) {

                        toastr.success('Produto cadastrado com sucesso!', 'Sucesso!', {timeOut: 3000});
                        $('#products_table').DataTable().ajax.reload(null, false); //faz o reaload da view datatable mas mantem a pagina atual

                        $('input').each(function () {
                            $(this).not('[name=_token]').val('');
                            $(this).removeClass('error');
                        });
                        $('.error').each(function () {
                            $(this).removeClass('error').addClass('disable');
                        })
                        $('body').loadingModal('destroy');
                        $('.modal').modal('hide')
                    },
                    error: function (data) {
                        toastr.error('Não foi possível cadastrar!', 'Erro!', {timeOut: 3000});
                    }
                });
            }
        });

        $('#form_update').validate({
            onkeyup: function (element) {
                $(element).valid();
            },
            errorClass: "error",
            rules: {
                description: {
                    required: true,
                    minlength: 3
                },
                price_cost: {
                    required: true,
                },
                unitary_value: {
                    required: true,
                },
                minimum_amount: {
                    required: true,
                },
            },
            messages: {
                description: {
                    required: "Por favor, informe uma descrição",
                    minlength: "A descrição deve ter no mínimo 3 caracteres."
                },
                price_cost: {
                    required: "Por favor, informe o preço de custo.",
                },
                unitary_value: {
                    required: "Por favor, informe Valor venda.",
                },
                minimum_amount: {
                    required: "Por favor, a Estoqueima.",
                },

            },
            submitHandler: function (form) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: '/adm/update/product',
                    data: $('#form_update').serialize(),

                    success: function (data) {

                        toastr.success('Produto editado com sucesso!', 'Sucesso!', {timeOut: 3000});
                        $('#products_table').DataTable().ajax.reload(null, false); //faz o reaload da view datatable mas mantem a pagina atual

                        $('input').each(function () {
                            $(this).not('[name=_token]').val('');
                            $(this).removeClass('error');
                        });
                        $('.error').each(function () {
                            $(this).removeClass('error').addClass('disable');
                        })
                        $('body').loadingModal('destroy');
                        $('.modal').modal('hide');
                    },
                    error: function (data) {
                        toastr.error('Não foi possível cadastrar!', 'Erro!', {timeOut: 3000});
                    }
                });
            }
        });



        function detail(id) {
            $('#ModalDetail').modal('show');
            $.ajax({
                type: 'GET',
                url: '/adm/detail/product/'+id,
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    $('#code_detail').val(data.code);
                    $('#code_for_detail').val(data.code_for);
                    $('#code_gru_detail').val(data.code_gru);
                    $('#description_detail').val(data.description);
                    $('#minimum_amount_detail').val(data.minimum_amount);
                    $('#model_one_detail').val(data.model_one);
                    $('#model_two_detail').val(data.model_two);
                    $('#name_gru_detail').val(data.name_gru);
                    $('#price_cost_detail').val(numberParaReal(parseInt(data.price_cost)));
                    $('#unitary_value_detail').val(numberParaReal(parseInt(data.unitary_value)));
                    $('#unity_detail').val(data.unity);
                    $('#weight_detail').val(data.weight);
                    $('body').loadingModal('destroy');
                }
            });
        }

        function edit(id) {
            $('#ModalUpdate').modal('show');
            $('#product_item').val(id);
            $.ajax({
                type: 'GET',
                url: '/adm/detail/product/'+id,
                beforeSend: function () {
                    loading();
                },
                success: function (data) {
                    $('#code_update').val(data.code);
                    $('#code_for_update').val(data.code_for);
                    $('#code_gru_update').val(data.code_gru);
                    $('#description_update').val(data.description);
                    $('#minimum_amount_update').val(data.minimum_amount);
                    $('#model_one_update').val(data.model_one);
                    $('#model_two_update').val(data.model_two);
                    $('#name_gru_update').val(data.name_gru);
                    $('#price_cost_update').val(numberParaReal(parseInt(data.price_cost)));
                    $('#unitary_value_update').val(numberParaReal(parseInt(data.unitary_value)));
                    $('#unity_update').val(data.unity);
                    $('#weight_update').val(data.weight);
                    $('body').loadingModal('destroy');
                }
            });
        }


        $(".valor").maskMoney({
            decimal: ",",
            thousands: "."
        });

        function numberParaReal(numero){
            var formatado = numero.toFixed(2).replace(".",",");
            return formatado;
        }

        function loading(txt) {
            if (txt)
                txt = txt;
            else
                txt = 'Aguarde enquanto processamos sua requisição...';
            $('body').loadingModal({
                position: 'auto',
                text: txt,
                color: '#fff',
                opacity: '0.7',
                backgroundColor: 'rgb(0,0,0)',
                animation: 'doubleBounce'
            });
        }

    </script>

@endsection
