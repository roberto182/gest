<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments();
            $table->string('code')->nullable();
            $table->string('description')->nullable();
            $table->string('unity')->nullable();
            $table->decimal('price_cost')->nullable();
            $table->decimal('unitary_value')->nullable();
            $table->string('model_one')->nullable();
            $table->string('model_two')->nullable();
            $table->string('code_for')->nullable();
            $table->string('code_gru')->nullable();
            $table->integer('minimum_amount')->nullable();
            $table->string('name_gru')->nullable();
            $table->decimal('weight')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
