<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Models\Products\Products;
use Illuminate\Http\Request;


class ProductController extends Controller
{

    

    public function store(Request $request)
    {
        $price_cost = str_replace(',', '.', str_replace('.', '', $request['price_cost']));
        $unitary_value = str_replace(',', '.', str_replace('.', '', $request['unitary_value']));
        $code = Products::orderBy('id', 'desc')->first();
        $code = intval($code->code)+1;
        //return response()->json($code);
        $product = Products::create([
            'code' => $code,
            'description' => $request['description'],
            'unity' => $request['unity'],
            'price_cost' => $price_cost,
            'unitary_value' => $unitary_value,
            'model_one' => $request['model_one'],
            'model_two' => $request['model_two'],
            'code_for' => $request['code_for'],
            'code_gru' => $request['code_gru'],
            'minimum_amount' => $request['minimum_amount'],
            'name_gru' => $request['name_gru'],
            'weight' => $request['weight']
        ]);
        return response()->json('ok');
    }



    public function update(Request $request)
    {
        $product = Products::where('id',$request['id'])->first();
        $price_cost = str_replace(',', '.', str_replace('.', '', $request['price_cost']));
        $unitary_value = str_replace(',', '.', str_replace('.', '', $request['unitary_value']));
        if($product){
            $product->code = $request['code'];
            $product->description = $request['description'];
            $product->unity = $request['unity'];
            $product->price_cost = $price_cost;
            $product->unitary_value = $unitary_value;
            $product->model_one = $request['model_one'];
            $product->model_two = $request['model_two'];
            $product->code_for = $request['code_for'];
            $product->code_gru = $request['code_gru'];
            $product->minimum_amount = $request['minimum_amount'];
            $product->name_gru = $request['name_gru'];
            $product->weight = $request['weight'];
            $product->save();
        }

        return response()->json($product);
    }

    public function detail($id)
    {
        $product = Products::find($id);
        return response()->json($product);
    }

    public function delete(Request $request){
        $product = Products::find($request['item_product']);
        if($product){
            $product->delete();
            return response()->json('ok');
        }
        return response()->json('error');
    }

    public function getData()
    {
        $products = Products::orderBy('id','DESC')->get();

        return datatables()->of($products)->addColumn('action', function ($query) {
            return '<div class="text-center"> 
                        <a href="#" class="link-simples " id="detail_'.$query->id.'" onclick="detail('.$query->id.')" 
                            data-toggle="modal">
                            <i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Detalhes Item"></i>
                        </a>
                        <span class="separaicon"> </span>
                        <a href="#" class="link-simples " id="edit_'.$query->id.'" onclick="edit('.$query->id.')" 
                            data-toggle="modal">
                            <i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Editar Item"></i>
                        </a>
                        <span class="separaicon"> </span>
                        <a href="#" class="link-simples " id="delete_'.$query->id.'" onclick="drop('.$query->id.')" 
                            data-description="'.$query->description.'" data-toggle="modal">
                            <i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Remover Item"></i>
                        </a>
                    </div>';
        })->make(true);
    }

}
