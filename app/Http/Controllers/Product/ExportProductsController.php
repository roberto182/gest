<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Models\Products\Products;
use Illuminate\Http\Request;


class ExportProductsController extends Controller
{

    public function export(Request $request)
    {
        $array = array_unique($request->id);
        //$array = json_decode(json_encode($array), true);
        $products = Products::whereIn('id',$array)->get();
        return response()->json($products);
    }

}
