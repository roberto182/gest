<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected
        $table = 'products';

    public $timestamps = false;

    protected $fillable = [
        'code', 'description', 'unity','price_cost','unitary_value','model_one','model_two','code_for','code_gru','minimum_amount',
        'name_gru','weight'
    ];


    protected
        $guarded = ['id'];

}
